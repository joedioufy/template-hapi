"use strict";
const assert = require("assert");
const Path = require("path");
const Server = require("../../server");

describe("User", () => {
  const internals = {};
  before(async () => {
    internals.manifest = {
      connections: [
        {
          host: "localhost",
          port: 0,
          labels: ["userTest"],
        },
      ],
      registrations: [
        {
          plugin: {
            register: "./ressources/user",
            options: {
              defaultUser: { id: "0", name: "default" },
            },
          },
        },
      ],
    };

    internals.composeOptions = {
      relativeTo: Path.resolve(__dirname, "../../"),
    };
  });
  describe("User.controller", () => {
    it("Get user by id should returns user object", async () => {
      Server.init(
        internals.manifest,
        internals.composeOptions,
        async (err, server) => {
          const userTest = server.select("userTest");
          const request = {
            url: "/users/1",
            method: "GET",
          };
          await userTest.inject(request, (res) => {
            assert.equal(res.result.id, 1);
            server.stop();
          });
        }
      );
    });

    it("Get user by string id should returns bad request", () => {
      Server.init(
        internals.manifest,
        internals.composeOptions,
        async (err, server) => {
          const userTest = server.select("userTest");
          const request = {
            url: "/users/string",
            method: "GET",
          };
          await userTest.inject(request, (res) => {
            assert.equal(res.statusCode, 400);
            server.stop();
          });
        }
      );
    });
  });
  before(async () => {});
});
